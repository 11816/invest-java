package lt.invest.java.library;

import java.util.ArrayList;
import java.util.List;

public class Book {
	private final String title;
	private final Genre genre;
	private final List<Author> authors;
	private final int releaseYear;

	public Book(String title, Genre genre, int releaseYear) {
		this(title, genre, new ArrayList<>(), releaseYear);
	}

	public Book(String title, Genre genre, List<Author> authors, int releaseYear) {
		this.title = title;
		this.genre = genre;
		this.authors = authors;
		this.releaseYear = releaseYear;
	}

	public String getTitle() {
		return title;
	}

	public Genre getGenre() {
		return genre;
	}

	public List<Author> getAuthors() {
		return List.copyOf(authors);
	}

	public int getReleaseYear() {
		return releaseYear;
	}

	public Author getPrimaryAuthor() {
		Author author = authors.get(0);
		return author != null ? author : Author.anonymous();
	}
}
