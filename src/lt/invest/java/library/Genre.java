package lt.invest.java.library;

import java.util.Objects;

public class Genre {
	private final String name;

	public Genre(String name) {
		this.name = name;
	}

	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Genre genre = (Genre) o;
		return name.equals(genre.name);
	}

	public int hashCode() {
		return Objects.hash(name);
	}
}
