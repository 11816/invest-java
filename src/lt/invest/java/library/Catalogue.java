package lt.invest.java.library;

import lt.invest.java.library.util.MultipleCompare;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Catalogue {
	private final Map<Genre, List<Book>> books;

	public Catalogue() {
		this(new ArrayList<>());
	}

	public Catalogue(List<Book> books) {
		this.books = books.stream().collect(Collectors.groupingBy(book -> new Genre(book.getTitle())));
	}

	private Stream<Book> bookStream() {
		return books.values().stream().flatMap(Collection::stream);
	}

	public List<Book> getBooks() {
		return bookStream().collect(Collectors.toList());
	}

	public List<Book> sort(Comparator<Book> comparator) {
		return bookStream().sorted(comparator)
		                   .collect(Collectors.toList());
	}

	public List<Book> sort() {
		return this.sort(new DefaultSort());
	}

	private static class DefaultSort implements Comparator<Book> {
		public int compare(Book first, Book second) {
			if(first == second) return 0;
			return MultipleCompare.first(first.getPrimaryAuthor().compareTo(second.getPrimaryAuthor()))
			                      .then(Integer.compare(first.getReleaseYear(), second.getReleaseYear()))
			                      .then(first.getTitle().compareTo(second.getTitle()))
			                      .getResult();
		}
	}

	public List<Genre> getGenres() {
		return new ArrayList<>(books.keySet());
	}

	public List<Book> getBooksInGenre(Genre genre) {
		return List.copyOf(books.get(genre));
	}

	public Catalogue addBook(Book book) {
		books.computeIfAbsent(book.getGenre(), _g -> new ArrayList<>()).add(book);
		return this;
	}
}
