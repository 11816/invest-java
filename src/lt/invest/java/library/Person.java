package lt.invest.java.library;

import lt.invest.java.library.util.MultipleCompare;

public class Person implements Comparable<Person>{
	private final String firstName;
	private final String lastName;
	private final int age;

	public Person(String firstName, String lastName, int age) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}

	public int compareTo(Person that) {
		if(this == that) return 0;
		return MultipleCompare.first(this.lastName.compareTo(that.lastName))
		                      .then(this.firstName.compareTo(that.firstName))
		                      .then(Integer.compare(this.age, that.age))
		                      .getResult();
	};

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public int getAge() {
		return age;
	}

	public Person clone() {
		return new Person(firstName, lastName, age);
	}
}
