package lt.invest.java.library.util;


public interface MultipleCompare {
	static MultipleCompare first(int result){
		if(result != 0) return new SolvedCompare(result);
		else return new UnsolvedCompare();
	};

	MultipleCompare then(int nextResult);
	int getResult();
}

class SolvedCompare implements MultipleCompare {
	private final int result;

	SolvedCompare(int result) {
		this.result = result;
	}

	public MultipleCompare then(int nextResult) {
		return this;
	}

	public int getResult() {
		return result;
	}
}

class UnsolvedCompare implements MultipleCompare {
	UnsolvedCompare(){}

	public MultipleCompare then(int nextResult) {
		if(nextResult != 0) return new SolvedCompare(nextResult);
		else return this;
	}

	public int getResult() {
		return 0;
	}
}
