package lt.invest.java.library;


import java.util.*;
import java.util.stream.Collectors;

public class Library {
	private final List<Reader> readers;
	private final Catalogue catalogue;
	private final Map<Book, Set<BookTakeout>> takeouts = new HashMap<>();

	public Library(List<Book> books, List<Reader> readers) {
		this.catalogue = new Catalogue(books);
		this.readers = readers;
	}

	public BookTakeout takeoutBook(Book book, Reader reader) {
		var takeout = new BookTakeout(book, reader);
		takeouts.computeIfAbsent(book, (_b) -> new HashSet<>()).add(takeout);
		reader.addInProgressBook(book);
		return takeout;
	}

	public void returnBookWithoutCompleting(Book book, Reader reader) {
		returnBookWithoutCompleting(new BookTakeout(book, reader));
	}

	public void returnBookWithoutCompleting(BookTakeout takeout) {
		takeouts.get(takeout.getBook()).remove(takeout);
		takeout.stopReadingBook();
	}


	public void finishBook(Book book, Reader reader) {
		finishBook(new BookTakeout(book, reader));
	}

	public void finishBook(BookTakeout takeout) {
		takeouts.get(takeout.getBook()).remove(takeout);
		takeout.finishBook();
	}

	public List<Reader> getReaders() {
		return readers.stream().sorted((a, b) -> Integer.compare(a.getCompletedBooksCount(), b.getCompletedBooksCount()))
		                       .collect(Collectors.toList());
	}

	public List<Genre> getGenres() {
		return catalogue.getGenres();
	}

	public Book getRandomBookInGenre(Genre genre) {
		var books = catalogue.getBooksInGenre(genre);
		return books.get(new Random().nextInt(books.size()));
	}

	public Book getRandomBookInGenre(Genre genre, Reader reader) {
		var books = catalogue.getBooksInGenre(genre);
		var completedBooks = reader.getCompletedBooks();
		var inProgressBooks = reader.getInProgressBooks();
		return books.stream().filter(book -> !completedBooks.contains(book))
		                     .filter(book -> !inProgressBooks.contains(book))
				             .skip(new Random().nextInt(books.size()))
                             .findFirst()
				             .orElse(null);
	}

	public Library addBook(Book newBook) {
		catalogue.addBook(newBook);
		return this;
	}

	public Library addReader(Reader newReader) {
		readers.add(newReader);
		return this;
	}
}


