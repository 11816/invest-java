package lt.invest.java.library;

import java.util.Objects;

public class BookTakeout {
	private final Book book;
	private final Reader reader;

	public BookTakeout(Book book, Reader reader) {
		this.book = book;
		this.reader = reader;
	}

	public Book getBook() {
		return book;
	}

	public Reader getReader() {
		return reader;
	}

	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		BookTakeout that = (BookTakeout) o;
		return Objects.equals(book, that.book) && Objects.equals(reader, that.reader);
	}

	public int hashCode() {
		return Objects.hash(book, reader);
	}

	public void stopReadingBook() {
		reader.stopReadingBook(book);
	}

	public void finishBook() {
		reader.finishBook(book);
	}
}
