package lt.invest.java.library;

import java.util.ArrayList;
import java.util.List;

public class Reader extends Person {
	private final List<Book> inProgressBooks;
	private final List<Book> completedBooks;

	public Reader(String firstName, String lastName, int age) {
		this(firstName, lastName, age, new ArrayList<>(), new ArrayList<>());
	}

	public Reader(String firstName, String lastName, int age, List<Book> inProgressBooks, List<Book> completedBooks) {
		super(firstName, lastName, age);
		this.inProgressBooks = inProgressBooks;
		this.completedBooks = completedBooks;
	}

	public Person clone() {
		return new Reader(getFirstName(), getLastName(), getAge(), List.copyOf(inProgressBooks), List.copyOf(completedBooks));
	}


	public List<Book> getInProgressBooks() {
		return inProgressBooks;
	}

	public List<Book> getCompletedBooks() {
		return completedBooks;
	}

	public int getCompletedBooksCount() {
		return completedBooks.size();
	}

	public Reader addInProgressBook(Book book) {
		inProgressBooks.add(book);
		return this;
	}

	public Reader addCompletedBook(Book book) {
		completedBooks.add(book);
		return this;
	}

	public Reader stopReadingBook(Book book) {
		inProgressBooks.remove(book);
		return this;
	}

	public Reader finishBook(Book book) {
		stopReadingBook(book);
		addCompletedBook(book);
		return this;
	}
}
