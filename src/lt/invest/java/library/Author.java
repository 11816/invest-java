package lt.invest.java.library;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Author extends Person {
	private final List<Book> writtenBooks;

	public Author(String firstName, String lastName, int age) {
		this(firstName, lastName, age, new ArrayList<>());
	}

	public Author(String firstName, String lastName, int age, List<Book> writtenBooks) {
		super(firstName, lastName, age);
		this.writtenBooks = writtenBooks;
	}

	public Person clone() {
		return new Author(getFirstName(), getLastName(), getAge(), List.copyOf(writtenBooks));
	}

	public static Author anonymous() {
		return new Author("Unknown Author", "", 0);
	}
}

